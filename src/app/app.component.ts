import { Component, OnDestroy, OnInit } from '@angular/core';
interface Coordinate {
  accuracy: number | null;
  heading: number | null;
  latitude: number | null;
  longitude: number | null;
  speed: number | null;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  coordinante!: GeolocationCoordinates;
  speed = 0;
  time: Date = new Date();
  latitude: any;
  longitude: any;
  heading: any;
  accuracy: any;
  error?: string;
  isDark: boolean = true;
  readonly kmhUnit = 3.6;
  log?: string;
  private watchId!: number;
  wakeLock: any;

  ngOnInit(): void {
    if (window.navigator) {
      this.watchId = window.navigator.geolocation.watchPosition(
        (position: GeolocationPosition) => {
          this.coordinante = position.coords;
          this.speed = Math.round(this.kmhUnit * (position.coords.speed || 0));
          this.latitude = position.coords.latitude || 0;
          this.longitude = position.coords.longitude || 0;
          this.heading = position.coords.heading || 0;
          this.accuracy = position.coords.accuracy || 0;
        },
        (error: GeolocationPositionError) => {
          this.error = error.message;
        },
        {
          enableHighAccuracy: true,
          timeout: 5000,
          maximumAge: 0,
        }
      );

      const anyNav: any = navigator;
      if ('wakeLock' in navigator) {
        anyNav['wakeLock']
          .request('screen')
          .then((value: any) => {
            this.log = 'wakeLock requested';
            this.wakeLock = value;
          })
          .catch((err: any) => {
            this.log = err.message;
          });
      }
    }

    setInterval(() => {
      this.time = new Date();
    }, 1000);
  }

  ngOnDestroy(): void {
    window.navigator.geolocation.clearWatch(this.watchId);
    this.wakeLock &&
      this.wakeLock.release().then(() => {
        this.wakeLock = null;
      });
  }

  switchTheme() {
    this.isDark = !this.isDark;
  }
}
